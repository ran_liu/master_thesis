#set up fortran compiler, compile flags and link flags
FC = gfortran
FFLAGS = -fbounds-check
LFLAGS =

OBJECTS = variables.o arguments.o random.o rnorm.o minim.o get_x.o get_price.o get_value.o informed.o period_profit.o cum_profit.o market_maker.o main.o

MODULES = variables.mod arguments.mod random.mod minim.mod

.PHONY: clean


main.out: $(MODULES) $(OBJECTS)
	$(FC) $(LFLAGS) $(OBJECTS) -o main.out libspline.a
	./main.out


%.o : %.f90
	$(FC) $(FFLAGS) -c $<


%.mod : %.f90
	$(FC) $(FFLAGS) -c $<

clean:
	rm -f $(OBJECTS) $(MODULES) main.out *~
