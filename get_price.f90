SUBROUTINE get_price(p, y_k, k)
  USE variables
  IMPLICIT NONE

  INTEGER, INTENT (IN) :: k
  REAL (KIND=8), INTENT (IN) :: y_k
  REAL (KIND=8), INTENT (OUT) :: p
  REAL (KIND=8) :: p_d

  
  CALL spline_quadratic_val (Q, y_grid, PR(:,k), y_k, p, p_d)
  
END SUBROUTINE get_price

