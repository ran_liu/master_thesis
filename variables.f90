MODULE variables
  IMPLICIT NONE
  
  INTEGER :: y, z

  
  !! Parameters: fixed value
  ! N: number of auctions
  ! Q: number of grid points for price and total order flow
  ! R: number of simulations of noise to get expected profits
  INTEGER, PARAMETER :: N = 10, Q = 11, R = 200
  REAL (KIND=8), PARAMETER :: alpha = 1, p_0 = 3011., v_bar = 3000., &
  sigma_0 = 30., sigma_u = 100., p_bar = (1.-alpha)*p_0, PI = ATAN(1.) * 4., deltaT = 1. / REAL(N - 1)
  REAL (KIND=8), DIMENSION (1:Q), PARAMETER :: p_grid = (/ (p_0*(1.-2.*alpha) + z *p_0*4.*alpha / REAL(Q-1), z=0,Q-1) /)
  ! Discretise P. p^1 = (1+alpha)*p_0, p^Q = (1-alpha)*p_
  REAL (KIND=8), DIMENSION (1:Q), PARAMETER :: y_grid = (/ (-10+z*2, z=0,Q-1)/)


  !! Static variables
  ! Price history, initial values set to p_0
  REAL (KIND=8), DIMENSION (0:N), SAVE :: p_h = (/ (p_0, z=1,N+1) /)
  ! Pricing rule (PR), initialised to the last closing prics. 
  ! Trading strategy (TS) cannot be declared this way, since the informed problem is also solve by different v's.
  REAL (KIND=8), DIMENSION (1:Q, 1:N), SAVE :: PR = &
  RESHAPE( (/(( (/ (0.9*p_0+0.01*p_0*(z-1),z=1,Q) /) ), y=1,Q)/), (/Q,N/) )
  ! Value function, initialised to 0
  REAL (KIND=8), DIMENSION (1:Q, 1:N), SAVE :: VF = RESHAPE( (/(0.,z=1,Q*N)/), (/Q,N/))
  
END MODULE variables

