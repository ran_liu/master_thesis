SUBROUTINE rnorm(data_simu, R, SIM)

  USE random, ONLY : random_normal
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: R, SIM
  REAL (KIND=8), DIMENSION (R, SIM), INTENT (out) :: data_simu
  INTEGER :: i, s


  DO s = 1, SIM
     Do i = 1, R
  !CALL random_seed()
        data_simu(i, s) = random_normal()
     END DO
  END DO

END SUBROUTINE rnorm
