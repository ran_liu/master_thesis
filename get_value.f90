SUBROUTINE get_value(pi_future, p_pre, k)
  USE variables
  IMPLICIT NONE

  INTEGER, INTENT (IN) :: k
  REAL (KIND=8), INTENT (IN) :: p_pre
  REAL (KIND=8), INTENT (OUT) :: pi_future
  REAL (KIND=8) :: pi_d

  CALL spline_quadratic_val (Q, p_grid, VF(:,k), p_pre, pi_future, pi_d)

END SUBROUTINE get_value
