SUBROUTINE get_x(x, p_pre, TS_k)
  USE variables
  IMPLICIT NONE

  REAL (KIND=8), INTENT (IN) :: p_pre
  REAL (KIND=8), INTENT (IN), DIMENSION (1:Q) :: TS_k
  REAL (KIND=8), INTENT (OUT) :: x
  REAL (KIND=8) :: x_d

  CALL spline_quadratic_val (Q, p_grid, TS_k, p_pre, x, x_d)

END SUBROUTINE get_x
