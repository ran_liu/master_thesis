SUBROUTINE period_profit(x_k, pi_k)
  USE variables
  USE arguments
  IMPLICIT NONE

  INTEGER :: s
  REAL (KIND=8), INTENT (IN) :: x_k(:)
  REAL (KIND=8), INTENT (OUT) :: pi_k
  REAL (KIND=8), DIMENSION (1:R) :: pi_s
  REAL (KIND=8) :: p
  
  pi_s(:) = 0.
  
     IF (p_grid(i_dup) > p_bar) THEN
     DO s = 1, R
        CALL get_price(p, x_k(1)+u(s, k_dup), k_dup)
        IF (p < p_bar) p = p_bar
        pi_s(s) = x_k(1) * (v - p)
     END DO
     END IF


  pi_k = - SUM(pi_s) / REAL(Q)
  
  
END SUBROUTINE period_profit
