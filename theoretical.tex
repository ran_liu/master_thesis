\documentclass{article}
\usepackage{amsmath} \usepackage{amsfonts} \usepackage{amssymb}\usepackage{mathtools}
\usepackage{bbm} %indicator function
\usepackage{accents}
\usepackage{calrsfs}
\DeclareMathAlphabet{\pazocal}{OMS}{zplm}{m}{n}
\newcommand{\La}{\mathcal{L}}
\newcommand{\Lb}{\pazocal{L}}
\DeclareMathOperator*{\Max}{max}
\DeclareMathOperator{\E}{\mathbb{E}} %expectation symbol
\newcommand*\dif{\mathop{}\!\mathrm{d}} %making differential symbol d not italic
\newcommand{\barbelow}[1]{\underaccent{\bar}{#1}}
\newcommand{\sign}{\text{sgn}}


\begin{document}

\title{Master's Thesis Issues}
\author{Ran Liu}
\date{\today}
\maketitle

\section{Baseline Model}
\subsection{Comments on the Kyle Model}
Kyle (1985) considered first the model in which a number of auctions take place sequentially. He solved the model in three steps. In the first step the problem of the Informed is considered. It is assumed that the price is linear in the current and past total order flows. A backward induction argument is used to prove that the profit function of the Informed is quadratic and that the trading strategy is also linear. In the second step the market efficiency condition is used to determine the pricing rule. In the last step, it is proved that the equations obtained in the previous steps have a unique solution which characterise the unique linear equilibrium.

In the case of continuous auction, Kyle {\em assumed} that a linear (both $p_t$ and $x_t$) equilibrium exists and then solved for the parameters. He made this assumption due to the analogous structure of the continuous auction case to the sequential auction case.

Similar argument cannot be made with a stopping time. In the sequential auction case, since $\tau$ is random, we cannot find a staring point for backward induction. In the continuous auction case, we cannot proceed as usual since we do not know whether $x_t$ is linear in equilibrium given that $p_t$ is linear.

\newpage

\subsection{The Problem of the Informed}
At the beginning of the trading day, the Informed observe liquidation value of the stock $\tilde{v}$. They set the trading strategy $\{x_t\}_0^1$ by maximising their expected profits conditional on $\tilde{v}$, taking the pricing rule of the market maker as given.
By the principle of optimality (whatever the initial state and initial decision are, the remaining decisions must constitute an optimal policy with regard to the state resulting from the first decision), solving the optimal control problem at the beginning of the day is equivalent to solving the maximising problem period by period. 
\begin{align}
   &\sup_{\{x_t\}_0^1} & &\E\Big[ \int_{t\in [0,1]} (\tilde{v}-p_t) \mathbbm{1} _{\{t \le \tau\}} \dif x_t \mid \tilde{v}, p_0\Big]
\\
   &\text{subject to} \notag \\
   & & & \dif p_t = \lambda_t(\dif x_t + \dif u_t) \\
   & & & \tau = min\{t; t\in [0,1], p_t = (1-\alpha) p_0\}\\
   & & & \dif x_t = \mu(t, x_t, p_t, \sigma_u, \Sigma_0, \tilde{v}) \dif t 
\end{align}
The trading strategy is deterministic, that is given $t, x_t, p_t, \sigma_u, \Sigma_0$ and $\tilde{v}$, the quantity traded by the Informed within $\dif t$, i.e.\ $\dif x_t$, is perfectly determined. The function $f(\cdot)$ is not restrictive.

\subsubsection{Solving the model by Kyle's method}
Following Kyle's method, we first try to work out the ex ante profits.
\begin{equation*}
\begin{aligned}
\int_{t\in [0,1]} (\tilde{v}-p_t) \mathbbm{1} _{\{t \le \tau\}} \dif x_t & = \Pr(\tau>1)\int_{t\in [0,1]} (\tilde{v}-p_t) \dif x_t+\Pr(\tau\leq 1)[\int_{t\in [0,\tau]} (\tilde{v}-p_t) \dif x_t]\\
& =  (1-\Pr(\tau\leq 1))\int_{t\in [0,1]} (\tilde{v}-p_t) \dif x_t+\Pr(\tau\leq 1)[\int_{t\in [0,\tau]} (\tilde{v}-p_t) \dif x_t]\\
& =  \int_{t\in [0,1]} (\tilde{v}-p_t) \dif x_t - \Pr(\tau\leq 1)\int_{t\in [\tau,1]} (\tilde{v}-p_t) \dif x_t
\end{aligned}
\end{equation*}

\begin{align*}
&\E\Big[ \int_{t\in [0,1]} (\tilde{v}-p_t) \mathbbm{1} _{\{t \le \tau\}} \dif x_t \mid \tilde{v}, p_0\Big]\\
=&\E \Big[\int_{t\in [0,1]} (\tilde{v}-p_t) \dif x_t \mid \tilde{v}, p_0 \Big]-\E\Big[\Pr(\tau\leq 1)\int_{t\in [\tau,1]} (\tilde{v}-p_t) \dif x_t\mid \tilde{v}, p_0 \Big]\\
=&\underbrace{\int_0^1\E \Big[ (\tilde{v}-p_t) \mu(t, x_t, p_t, \sigma_u, \Sigma_0, \tilde{v}) \mid \tilde{v}, p_0 \Big]\dif t}_\text{term 1} -\underbrace{\E\Big[\Pr(\tau\leq 1)\int_\tau^1 (\tilde{v}-p_t) \mu(t, x_t, p_t, \sigma_u, \Sigma_0, \tilde{v})\dif t\mid \tilde{v}, p_0 \Big]}_\text{term 2}
\end{align*}



In Kyle (1985), $x_t$ is assumed to be linear, so that we can write $\mu(t, x_t, p_t, \sigma_u, \Sigma_0, \tilde{v})=\beta_t(\tilde{v}-p_t)$. Thus term 1 is reduced to $\int_0^1 \beta_t \Sigma_t^* \dif t$, which can be expressed by $\lambda_t, \Sigma_t^*$ and $\sigma_u^2$, where $\Sigma_t^*=\E[(\tilde{v}-p_t)^2\mid \tilde{v}, p_0]$. But here, term 1 cannot be further reduced. Term 2 does not appear in the original Kyle model.

\begin{itemize}
\item Probability of stopping trading
\end{itemize}
By reflection principle, we have
$$\Pr(\tau\leq 1, p_1 \geq p \mid p_0) = \Pr(\tau \leq 1, p_1 \leq 2\barbelow{p} - p\mid p_0)=\Pr(p_1 \leq 2\barbelow{p} - p\mid p_0).$$
Let $p=\barbelow{p}$, then
$$\Pr(\tau\leq 1\mid p_0)=\Pr(\tau\leq 1, p_1 \leq \barbelow{p}\mid p_0) + \Pr(\tau\leq 1, p_1 \geq \barbelow{p}\mid p_0) = 2\Pr(p_1 \leq \barbelow{p}\mid p_0).$$ 
Combining equations (2) and (4), we have
$$\dif p_t = \lambda_t \mu(t, x_t, p_t, \sigma_u, \Sigma_0, \tilde{v}) \dif t + \lambda_t \dif u_t.$$
We can either work out the probability distribution of $p_t$ or transform $p_t$ into a Brownian with drift. Both ways are difficult to implement since we do not know the functional form of $\mu(\cdot)$.

\begin{itemize}
\item Linear case
\end{itemize}
Assume $\mu(\cdot)$ is linear in $p_t$ and does not depend on $x_t$. Let $\mu(t, x_t, p_t, \sigma_u, \Sigma_0, \tilde{v})=\beta_t(\tilde{v}_-p_t)$. Then the price follows an Ornstein-Uhlenbeck process with time-dependent coefficients,
$$\dif p_t = \lambda_t \beta_t (\tilde{v}-p_t) \dif t + \lambda_t \dif u_t.$$
The explicit form of $p_t$ is
$$p_t=\psi_0(t)\big[p_0+\tilde{v}\int_0^t \frac{\lambda_s\beta_s}{\psi_0(s)} \dif s + \int_0^t \frac{\lambda_s}{\psi_0(s)}\dif u_s\big],$$
where
$$\psi_s(t) \coloneqq \exp \big\{-\int_s^t \lambda_r\beta_r \dif r\big\}, \text{ with } s \leq t.$$
Thus we can get the mean function conditional on $p_0$ for $p_t$,
$$m(t;p_0) \coloneqq \E [p_t\mid p_0] = \psi_0(t) \Big(p_0+\tilde{v}\int_0^t \frac{\lambda_s\beta_s}{\psi_0(s)} \dif s \Big).$$
The variance function is
$$var(p_t\mid p_0,\tilde{v})=\psi_0^2(t)\int_0^t (\frac{\lambda_s}{\psi_0(s)})^2\dif s.$$
Thus
$$p_t \mid p_0, \tilde{v} \sim N\Bigg(\psi_0(t) \Big(p_0+\tilde{v}\int_0^t \frac{\lambda_s\beta_s}{\psi_0(s)} \dif s \Big),\psi_0^2(t)\int_0^t (\frac{\lambda_s}{\psi_0(s)})^2\dif s\Bigg)$$
The probability density is
$$\Pr (p_1 \leq \barbelow{p} \mid \tilde{v},p_0) = \frac{1}{\psi_0(1)\sqrt{4\pi\gamma(0,1)}}\exp \Big\{-\frac{(\frac{\barbelow{p}}{\psi_0(1)}+\theta(0,1)-p_0)^2}{4\gamma(0,1)}  \Big\},$$
where
$$\theta(s,t)=m(s)-\frac{m(t)}{\psi_s(t)}, \theta(0,1)=-\tilde{v}\int_0^1\frac{\lambda_s\beta_s}{\psi_0(s)}\dif s, \gamma(s,t) = \int_s^t \frac{\sigma^2}{2\psi_s^2(u)} \dif u.$$
Therefore, the objective function becomes
$$\E\Big[ \int_{t\in [0,1]} (\tilde{v}-p_t) \mathbbm{1} _{\{t \le \tau\}} \dif x_t \mid \tilde{v}, p_0\Big]=\int_0^1 \beta_t \Sigma_t^* \dif t-2\Pr (p_1 \leq \barbelow{p} \mid \tilde{v},p_0)\E\Big[\int_\tau^1 \beta_t (\tilde{v}-p_t)^2 \dif t \mid \tilde{v}, p_0\Big].$$
Taking from Kyle (1985), 
$$\int_0^1 \beta_t \Sigma_t^* \dif t=\frac{1}{2}\int_0^1\lambda_t\sigma_u^2\dif t +\frac{1}{2}\int_0^1\lambda^{-1}_t\dif(-\Sigma^*_t).$$


Following Kyle (1985), we can argue that $\lambda_t$ can only be constant. (if $\lambda_t$ ever increases, then unbounded profits can be generated by increasing $\Sigma_t$ then decreasing it; price no longer fluctuates at the end of the trading)

\newpage
\subsubsection{Solving the control problem by verification}

To put the maximisation into the stochastic control problem context, we view $\{x_t\}_0^1$ as the control process. The controlled process is then defined by equation (2). In the notation of Touzi (p.\ 55-56), the controlled process and the gain criterion are
$$\dif X_t^\nu = b(t, X_t^\nu, \nu_t)\dif t + \sigma(t,X_t^\nu,\nu_t)\dif W_t,$$
and
$$J(t_0,x,\nu)=\E\big[\int_{t_0}^T \beta^\nu(t_0,t)f(t,X_t^{t_0,x,\nu},\nu_t)\dif t + \beta^\nu(t_0,T)g(X_T^{t_0,x,\nu})\big],$$
respectively, with $X_t^\nu = p_t, b(t, X_t^\nu, \nu_t)=\lambda_t \mu(t, x_t, p_t, \sigma_u, \Sigma_0, \tilde{v})$ and $\sigma(t,X_t^\nu,\nu_t)=\lambda_t$. We rewrite our objective function as
$$\E\big[ \int_0^1 (\tilde{v}-p_t)  \mu(t, x_t, p_t, \sigma_u, \Sigma_0, \tilde{v}) \mathbbm{1} _{\{t \le \tau\}}\dif t|\tilde{v}, p_0\big],$$
so that $g(\cdot)=0, \beta^\nu(t_0,t) = e^{-\int_{t_0}^t k(r,X_r^{t_0,x,\nu},\nu_r)\dif r}=(\tilde{v}-p_t)  \mu(t, x_t, p_t, \sigma_u, \Sigma_0, \tilde{v})$ and $f(t,X_t^{t_0,x,\nu},\nu_t)= \mathbbm{1} _{\{t \le \tau\}}$. One problem is to solve for $k(\cdot)$.
$$e^{-\int_{t_0}^t k(r,X_r^{t_0,x,\nu},\nu_r)\dif r}=(\tilde{v}-p_t)  \mu(t, x_t, p_t, \sigma_u, \Sigma_0, \tilde{v}) \implies $$
$$k(t,X_r^{t_0,x,\nu},\nu_r)\dif t=\frac{\dif p_t}{\tilde{v}-p_t}-\frac{\frac{\partial \mu(\cdot)}{\partial p_t}\dif p_t}{\mu(t, x_t, p_t, \sigma_u, \Sigma_0, \tilde{v})},$$
which is potentially explosive. The Hamiltonian is
$$ H(t,p,r,q,\gamma)= \sup_x \big\{ -k(t,p,x)r+ \lambda_t \mu(t, x, p, \sigma_u, \Sigma_0, \tilde{v})q+\frac{1}{2}\lambda_t^2\gamma + \mathbbm{1} _{\{t \le \tau\}} \big\}.$$
Let $V(t,p)$ denotes the value function. Then the associated Hamilton-Jacobi-Bellman equation is
$$-\partial_t v - H(t, p, v, Dv, D^2v)=0 \text{ and } v(T,\cdot)=0.$$
From this we try to solve $v$.
$$\frac{\partial v}{\partial t}(t, p) + \sup_x \Lb^x v(t,p)=0,$$
where in the linear case
$$\Lb^x v(t,p)=-k(t,p,x)v(t,p)+\lambda_t\beta_t(\tilde{v}-p_t)\frac{\partial v}{\partial p}(t, p)+\frac{1}{2}\lambda_t^2\frac{\partial^2 v}{\partial p^2}(t, p).$$

\subsection{The Problem of the Market Maker}
The market efficiency condition declares that the pricing rule must satisfy $p_t = \E\big[\tilde{v} \mid \{\dif x_t + \dif u_t\}_{t\in [0,\tau]}\big]$. From $t=\tau$ on, no transaction is carried out. So ex ante the market dealer does not care whether $\tau$ occurs before $t$.
\end{document}
