\documentclass{article}
\usepackage{amsmath} \usepackage{amsfonts} \usepackage{amssymb}\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{bbm} %indicator function
\usepackage{accents}
\usepackage{calrsfs}
\DeclareMathAlphabet{\pazocal}{OMS}{zplm}{m}{n}
\newcommand{\La}{\mathcal{L}}
\newcommand{\Lb}{\pazocal{L}}
\DeclareMathOperator*{\Max}{max}
\DeclareMathOperator{\E}{\mathbb{E}} %expectation symbol
\newcommand*\dif{\mathop{}\!\mathrm{d}} %making differential symbol d not italic
\newcommand{\barbelow}[1]{\underaccent{\bar}{#1}}
\newcommand{\sign}{\text{sgn}}
\usepackage{float}
    
\usepackage{xcolor}

\newcommand\overmat[2]{%
  \makebox[0pt][l]{$\smash{\color{white}\overbrace{\phantom{%
    \begin{matrix}#2\end{matrix}}}^{\text{\color{black}#1}}}$}#2}
\newcommand\partialphantom{\vphantom{\frac{\partial e_{P,M}}{\partial w_{1,1}}}}

\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Fortran,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}



\begin{document}

\title{Solving the model numerically}
\author{Ran Liu}
\date{\today}
\maketitle

\section{Model and Solution}

We consider the sequential auctions on the time interval $[0,1]$. Auctions take place $t_n$, where $n\in \{1,\dots,N\}, t_1=0, t_N =1$. Auctions start at $t_1$. However, all investors observe $p_0$, which can be interpreted as the closing price from the previous trading day. To be consistent, the subscript $n$ of variables and functions denotes the $n^{th}$ auction. The superscript of variables $p$ and $y$ below describes the discretisation grid. At the $n^{th}$ auction, the informed investor places an order $x_n$, the noise trader place $u_n$, and the total order flow is $y_n=x_n+u_n$.
 
When determining the current bid, the informed investor observes $\tilde{v}$ and takes the past prices as given, that is the trading strategy is $x_n(p_1, \dots, p_{n-1}; \tilde{v})$. We assume the Markovian property, that is $x_n(p_1, \dots, p_{n-1}; \tilde{v}) = x_n(p_{n-1}; \tilde{v})$. We discretise $p$ into $Q$ points and evaluate the $N$ trading strategies $\{x_n\}_{n=1}^N$ at those points, respectively.

\begin{equation*}
\underbrace{\begin{pmatrix}
\overmat{$t_0$}{p^1} & \overmat{$t_1$}{p^1} & \cdots & \overmat{$t_{N-1}$}{p^1} \\
p^2 & p^2 & \cdots & p^2 \\
\vdots & \vdots & \ddots & \vdots \\
p^Q & p^Q & \cdots & p^Q
\end{pmatrix}_{Q\times N}}_{(1)}
\implies
\underbrace{\begin{pmatrix}
\overmat{$t_1$}{x_1(p^1)} & \overmat{$t_2$}{x_2(p^1)} & \cdots & \overmat{$t_N$}{x_N(p^1)} \\
x_1(p^2) & x_2(p^2) & \cdots & x_N(p^2) \\
\vdots & \vdots & \ddots & \vdots \\
x_1(p^Q) & x_2(p^Q) & \cdots & x_N(p^Q)
\end{pmatrix}_{Q\times N}}_{(2)}
\end{equation*}

At the market maker side, he takes past and current total order flows as given and set the current price, that is the pricing rule is the function $p_n(y_1,\dots,y_n)$. Again, we assume the Markovian property, that is $p_n(y_1,\dots,y_n)=p_n(y_n)$.

\begin{equation*}
\underbrace{\begin{pmatrix}
\overmat{$t_1$}{y^1}& \overmat{$t_2$}{y^1} & \cdots & \overmat{$t_N$}{y^1} \\
y^2 & y^2 & \cdots & y^2 \\
\vdots & \vdots & \ddots & \vdots \\
y^Q & y^Q & \cdots & y^Q
\end{pmatrix}_{Q\times N}}_{(3)}
\implies
\underbrace{\begin{pmatrix}
\overmat{$t_1$}{p_1(y^1)} & \overmat{$t_2$}{p_2(y^1)} & \cdots & \overmat{$t_N$}{p_N(y^1)} \\
p_1(y^2) & p_2(y^2) & \cdots & p_N(y^2) \\
\vdots & \vdots & \ddots & \vdots \\
p_1(y^Q) & p_2(y^Q) & \cdots & p_N(y^Q)
\end{pmatrix}_{Q\times N}}_{(4)}
\end{equation*}


\subsection{The informed investor}
\subsubsection{The maximisation problem}
At the $n^{th}$ auction, the informed investor solves the following problem:
\begin{align}
   &\Max_{\{x_k\}_n^N} & &\E\Big[ \Pi_n\mid p_{n-1}\Big]=\E\Big[\sum_{k=n}^N\tilde{\pi}_k\mid p_{n-1}\Big] \notag
\\
   &\text{subject to} \notag \\
   & & & \tilde{\Pi}_k = (v - \tilde{p_k}) x_k \mathbbm{1}(p_{n-1}>\barbelow{p})+\tilde{\Pi}_{k+1},  \text{ } k = n, n+1, \dots, N-1\notag\\\
   & & & \tilde{p}_k = p_k(x_k+\tilde{u}_k, p_{k-1})=
   \begin{cases}
   max\big\{\hat{p}_k(x_k+\tilde{u}_k), \barbelow{p}\big\}, & \text{ if }  p_{k-1}>\barbelow{p}\\
   \barbelow{p}, & \text{ if }  p_{k-1}\leq\barbelow{p}
   \end{cases}\notag
\end{align}
where $\Pi_n$ is the sum of current and future profits and $\pi_n$ is the current profit at the $n^{th}$ auction. Note that $v$ is without tilde because the informed investor observes the true value of the asset at the beginning of the day, and $p_{n-1}$ is without tilde because it is known at the $n^{th}$ auction to every one. Note also that the pricing rule $p_k(\cdot,\cdot)$ is a mapping from $(y_k, p_{k-1})$ to $p_k$, and $\hat{p}_k(\cdot)$ is a mapping from $y_k$ to $p_k$. The informed investor has belief about the pricing rules, that is when solving the problem, the mappings $p_k(y_k, p_{k-1})$, $k=n,\cdots,N$ are known.

\subsubsection{Solution}
This problem can be solved by backward induction. At period $k$, the state variable is $p_{k-1}$, and the control variable is $x_k$.

Let $V_k \coloneqq \E[\tilde{\Pi}_k\mid p_{k-1}]$. At the $N^{th}$ auction, 
$$V_N = \E[(v-\tilde{p}_N)x_N\mathbbm{1}(p_{N-1}>\barbelow{p})\mid p_{N-1}]=0.$$
Note that we have either $v=\tilde{p}_N$, which is the assumption that at the end of the day the true value of the asset is revealed, or $\mathbbm{1}(p_{N-1}>\barbelow{p})=0$, that is the lower bound of the price has been reached before the end of the day. In either case, no matter what $x_N$ the informed investor choose, we must have $V_N=0$. Without loss of generality, hereinafter we set $x_k=0$ whenever its value does not matter. 

At the $(N-1)^{th}$ auction, 
\begin{equation*}
\begin{aligned}
V_{N-1} = &\E[(v-\tilde{p}_{N-1})x_{N-1}\mathbbm{1}(p_{N-2}>\barbelow{p})\mid p_{N-2}] + V_N\\
              = &\E[(v-\tilde{p}_{N-1})x_{N-1}\mathbbm{1}(p_{N-2}>\barbelow{p})\mid p_{N-2}]\\
              = & x_{N-1}\mathbbm{1}(p_{N-2}>\barbelow{p})(v-\E[\tilde{p}_{N-1}\mid p_{N-2}])\\
              = & x_{N-1}\mathbbm{1}(p_{N-2}>\barbelow{p})\Big(v-\E[p_{N-1}(x_{N-1}+\tilde{u}_{N-2}, p_{N-2})\mid p_{N-2}]\Big)\\
              = & \begin{cases}
              x_{N-1}\Big(v-\E[\max \{\hat{p}_{N-1}(x_{N-1}+\tilde{u}_{N-2}), \barbelow{p}\}]\Big),& \text{ if } p_{N-2}>\barbelow{p}\\
              0, &  \text{ if } p_{N-2}\leq\barbelow{p}
              \end{cases}
\end{aligned}
\end{equation*}
We can simulate $u_{N-2}$ and solve numerically for the $x_{N-1}$ that maximises $V_{N-1}$. Let us denote the optimal bidding volume $x^*_{N-1}$ and the maximised profits $V^*_{N-1}$.

At the $(N-2)^{th}$ auction, we have the following Bellman equation
\begin{equation*}
\begin{aligned}
V^*_{N-2} = & \Max_{x_{N-2},x_{N-1}} \Bigg\{x_{N-2}\mathbbm{1}(p_{N-3}>\barbelow{p})\Big(v-\E[p_{N-2}(x_{N-2}+\tilde{u}_{N-2}, p_{N-3})\mid p_{N-3}] \Big)+ V_{N-1}\Bigg\}\\
=& \Max_{x_{N-2}} \Bigg\{x_{N-2}\mathbbm{1}(p_{N-3}>\barbelow{p})\Big(v-\E[p_{N-2}(x_{N-2}+\tilde{u}_{N-2}, p_{N-3})\mid p_{N-3}] \Big)+V^*_{N-1}(p^*_{N-2}(y_{N-2}))\Bigg\}.
\end{aligned}
\end{equation*}
where the second equal sign is due to the Principle of Optimality, i.e.\ ``An optimal policy has the property that whatever the initial state and initial decision are, the remaining decisions must constitute an optimal policy with regard to the state resulting from the first decision" (Bellman, 1957, Chap. III.3.).

We can work backwards in this way until the beginning of the day.

\subsection{The market maker}
\subsubsection{Market efficiency condition}
At period $k$, the market efficiency condition requires that the market maker to the set the price to the expected value of the asset condition on the information he has, that is 
\begin{equation*}
p_k=\begin{cases}
\E[\tilde{v}\mid \tilde{y_k}, p_{k-1}]=\E[\tilde{v}\mid x_k(p_{k-1};\tilde{v})+\tilde{u}_k, p_{k-1}], & \text{ if } p_{k-1} > \barbelow{p}\\
\barbelow{p}, & \text{ if } p_{k-1} \leq \barbelow{p}
\end{cases}
\end{equation*}
\subsubsection{Solution}
The market maker observes the state $p_{k-1}$, and has a belief about the informed investor's current-period trading strategy $x_k(p_{k-1};\tilde{v})$, but he does not observe $\tilde{v}$.

Using Bayes' Rule, we have
$$f(\tilde{v}\mid x_k(p_{k-1};\tilde{v})+\tilde{u}_k)=
\frac{f(x_k(p_{k-1};\tilde{v})+\tilde{u}_k\mid \tilde{v})f(\tilde{v})}{f(x_k(p_{k-1};\tilde{v})+\tilde{u}_k)}=
\frac{f(x_k(p_{k-1};\tilde{v})+\tilde{u}_k\mid \tilde{v})f(\tilde{v})}{\int_{-\infty}^{+\infty} f(x_k(p_{k-1};\tilde{v})+\tilde{u}_k\mid \tilde{v} )f(\tilde{v})\dif \tilde{v}}.$$ 
The market maker knows the distribution of $\tilde{v}$ and $\tilde{u}_k$, and the function $x_k(p_{k-1};\tilde{v})$. Note that $f(x_k(p_{k-1};\tilde{v})+\tilde{u}_k\mid \tilde{v})$ is the pdf of $N(x_k(p_{k-1};\tilde{v}),\sigma_u^2\Delta t)$. The RHS is known. 
\begin{equation*}
\begin{aligned}
&\Pr(v^i-\epsilon < \tilde{v} \leq v^i + \epsilon\mid x_k(p_{k-1};\tilde{v})+\tilde{u}_k) \\
=  &\int_{v^i - \epsilon}^{v^i + \epsilon} f(\tilde{v}\mid x_k(p_{k-1};\tilde{v})+\tilde{u}_k) \dif \tilde{v}
=  \frac{\int_{v^i - \epsilon}^{v^i + \epsilon}f(x_k(p_{k-1};\tilde{v})+\tilde{u}_k\mid \tilde{v})f(\tilde{v})\dif \tilde{v}}{\int_{-\infty}^{+\infty} f(x_k(p_{k-1};\tilde{v})+\tilde{u}_k\mid \tilde{v} )f(\tilde{v})\dif \tilde{v}}\\
\approx & \frac{2\epsilon f(x_k(p_{k-1};v_i)+\tilde{u}_k\mid \tilde{v}=v_i)f(v_i)}{\sum_{i=1}^Q 2\epsilon f(x_k(p_{k-1};v_i)+\tilde{u}_k\mid \tilde{v}=v_i)f(v_i)}
= \frac{ f(x_k(p_{k-1};v_i)+\tilde{u}_k\mid \tilde{v}=v_i)f(v_i)}{\sum_{i=1}^Q f(x_k(p_{k-1};v_i)+\tilde{u}_k\mid \tilde{v}=v_i)f(v_i)}
\end{aligned}
\end{equation*}



%Since $\tilde{v}$ remains random for the market maker, and $x_k(p_{k-1};\tilde{v})$ is likely to depend nonlinearly on $\tilde{v}$, we cannot find out this conditional expectation analytically or using linear regression. We try nonparametric regression.
%
%There are many options by which we can do this. As the first try, we use a simple method. We adopt the Nadaraya-Watson kernel estimator for the conditional expectation function
%$$\hat{\E}[\tilde{v}\mid y_k=y^i]=\frac{\sum_{j=1}^R v^{(j)} K(\frac{y^i-y^{(j)}}{h})}{\sum_{j=1}^R K(\frac{y^i-y^{(j)}}{h})},$$
%where $h$ is the bandwidth parameter, and we set it to be $Q^{-\frac{1}{5}}$ by the ``rule of thumb". And we use the standard normal density for $K(\cdot)$.


\subsection{Equilibrium}
We use iterations to find the equilibrium trading strategy and pricing rule. We start from the informed investor, next go to the market maker problem, and then go back to the informed investor problem, starting the iteration.

For the first iteration, we first discretise the state variable for each period, that is $p_{k-1}$, where $k=1,\cdots,N$, into $Q$ points as shown in matrix (1). Then we let the informed investor pick up an arbitrary set of pricing rules, $(p_1(y_1), p_2(y_2), \cdots, p_N(y_N))^T$. We can interpret the iteration looking for equilibrium as the process of forming the correct belief about the other player for the informed investor and the market maker. After a Monte Carlo simulation for $\{u_k\}_1^N$, we can solve the informed investor problem and get the trading strategy vector $(x_1(p_1), x_2(p_2),\dots, x_N(p_N))^T$ as output. Note that for each function in the trading strategy vector, we only get its valuations of $Q$ points.

Now we continue to the market maker side. First we discretise the total order flow into $Q$ points, as shown by matrix (3). At each period, the market maker has to set the price according to market efficiency condition. Given the distribution of $(x_1(p_1;\tilde{v}), x_2(p_2;\tilde{v}),\dots, x_N(p_N;\tilde{v}))^T$ and $(\tilde{u}_1,\tilde{u}_2,\cdots,\tilde{u}_N)^T$,  the market maker knows the distribution of $\tilde{y}_k, k=1,2,\cdots,N$. Then he can work out the conditional expectations $p_k=\E[\tilde{v}\mid\tilde{y_k}]$ at the given $Q$ points.

After the first iteration, we get a new set of pricing rules than the initial arbitrary pricing rules we fed into the informed investor problem in the first iteration. We compare these pricing rules. From the second iteration on, we also compare the new set of trading strategies from those of the previous iteration. When the trading strategies and the pricing rules from two consecutive iterations are very close, we stop the iteration.

\subsection{Argument against Markovian}
The bad shock might matter more as we get closer to the end of the day.


\section{Implementation}

\subsection{Static call graph}
\begin{figure}[H]
             \centering
             \includegraphics[width=1.2\columnwidth]{call_graph}

 \end{figure}
 
 \subsection{Main}
 The basic structure is two loops. The outer loop traverses time, and the inner loop uses iteration looking for equilibrium.
 \begin{lstlisting}
 PROGRAM main
  USE Parameters
  IMPLICIT NONE

  INTEGER :: s, k
  REAL (KIND=8) :: deltaT, t, v, tol = 10D-4, u
  REAL (KIND=8), DIMENSION (1:Q, 1:N) :: TS, TS_pre, PR, PR_pre


  DO k = 1, 1
     !Outer loop, traversing period (auctions)
     
     CALL rnorm(v, 1, 1)  !simulate standard normal
     v = sigma_0 * v + v_bar !!realise the value of the asset 

     !Give arbitrary initial value for TS and PR
     TS = RESHAPE( (/ (20,i=1,Q*N) /), (/Q,N/) )
     PR = RESHAPE( (/ (p_0,i=1,Q*N) /), (/Q,N/) )
     
     
     DO i=1,1 !should delete i=1,1
        !Mid loop, looking for the equilibrium for each auction

        !Start from the informed investor problem
        !Output: Trading strategy (TS)
        CALL informed(TS(:,k:N), PR(:,k:N), v, k)

        
        !Proceed to the marker maker problem
        Call market_maker(PR(:,k:N), TS(:,k:N), k)

        !Check if we TS and PR converge
        IF ( (MAXVAL(TS-TS_pre) < tol) .AND. (MAXVAL(PR-PR_pre) < tol) ) EXIT
        !Record TS and PR for comparison in the next iteration
        TS_pre(:,k:N) = TS(:,k:N)
        PR_pre(:,k:N) = PR(:,k:N)

     END DO
     
  END DO

END PROGRAM main
\end{lstlisting}
 
 \subsection{The informed investor}
SUBROUTINE \verb|informed(TS, PR, v, k)| takes the true value of the stock $v$, the current period $k$, and the pricing rules $PR_{Q\times N}$ as input, and produces the trading strategy $TS_{Q\times N}$. Note that at period $k$, we actually obtain the sequence $x_m(p_{m-1}), m \in \{k, \dots, N\}$. But we take into consideration the possibility of regrets. So we update the $k^{th}$ to the $N^{th}$ columns of $TS$.
\begin{lstlisting}
DO j = N-1, 1, -1
     !backward induction                                                        
     !N-1 is de facto the last period, with a termination value V_N=0           

     IF (j == N-1) THEN
	CALL solvopt(1, x_k, val, period_profit, .FALSE., null, options, .FALSE\
., .FALSE.)
     ELSE
        CALL solvopt(1, x_k, val, objf, .FALSE., null, options, .FALSE., .FALSE\
.)
     END IF
  END DO
\end{lstlisting}

Monte Carlo for $u_k$'s is done in SUBROUTINE \verb|period_profit| when calculating expected current period profit. Interpolation is need to get a price from $p_k(y_k)$ when sweeping $x_k$.
\begin{lstlisting}	
SUBROUTINE period_profit(pi_k, x_k, PR, p_pre, v, k)
  USE Parameters
  IMPLICIT NONE

  INTEGER, INTENT (IN) :: k
  REAL (KIND=8), INTENT (IN) :: x_k, p_pre, v
  REAL (KIND=8), DIMENSION (1:Q), INTENT (IN) :: PR
  REAL (KIND=8),  INTENT (OUT) :: pi_k
  REAL (KIND=8), DIMENSION (1:R) :: u, pi_s

  !draw a sample of u to form total order flow                                  
  CALL rnorm(u, R, 1)
  u = sigma_u * u * SQRT(deltaT)

  DO i = 1, R
     IF (p_pre > p_bar) THEN
	CALL get_price(p, x_k+u(i), PS, k)
        IF (p < p_bar) p = p_bar
	pi_s(i) = x_k * (v - p)
     ELSE
        pi_s(i) = 0
     END IF
  END DO
  
  pi_k = SUM(pi_s) / Q
END SUBROUTINE period_profit
\end{lstlisting}
 
 \subsection{The market maker}
 SUBROUTINE \verb|market_maker.f90| is used to solve the market maker problem.
 
 There is one complication. The market maker knows the form of $x_k(p_{k-1};\tilde{v})$ but not $\tilde{v}$. Since we do not have analytical solution, we cannot simply plug in possible realisations of $\tilde{v}$. We let the market maker solve the informed investor problem again using different possible realisations of $\tilde{v}$. And then simulate the noise $u_k$. When doing the conditional expectation, we only use the sum of $x_k$ and $u_k$. Note that calling \verb|informed.f90| $R$ times at each period is computationally costly.
 
 First we can discretise a grid for $\tilde{v}$, then for each point resolve the market maker problem to get $x_k(p_{k-1};\tilde{v})$, simulate $\tilde{u}_k$ and finally compute the RHS. Then the LHS can be used to compute the conditional expectation.
 
 \begin{lstlisting}

  \end{lstlisting}


$f(x+u\mid v)= f(v\mid x+u)\frac{f(x+u)}{f(v)}$
start from 5 grid100 
running time
y against n
x against p
p against y

3-4 slides of motivation
pics China
policy beginning of the year
NY times/Bloomberg/FT
why stops trading

related literature, Kyle, those cite Kyle
  
%\section{Results}

\end{document}
