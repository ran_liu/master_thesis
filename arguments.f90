MODULE arguments
  USE VARIABLES
  IMPLICIT NONE
  
  INTEGER, SAVE :: k_dup, i_dup=1 ! index of grid points
  REAL (KIND=8), SAVE :: v
  REAL (KIND=8), DIMENSION (1:R, 1:N), SAVE :: u
  
END MODULE arguments
