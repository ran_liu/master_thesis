SUBROUTINE informed(TS, k)
  USE variables
  USE arguments
  USE Nelder_Mead
  IMPLICIT NONE

  INTEGER, INTENT (IN) :: k
  REAL (KIND=8), DIMENSION (1:Q, k:N), INTENT (IN OUT) :: TS
  INTEGER :: i, j
  INTEGER, PARAMETER :: iprint = -1, nop = 1
  REAL (KIND=8)          :: object, x_k(1), simp=1.d-4, step(1), stopcr=1.d-04, var(1)
  INTEGER            :: ier, iquad=0, maxf, nloop=6
  LOGICAL            :: first

  INTERFACE
     SUBROUTINE period_profit(x_k, pi_k)
       IMPLICIT NONE
       REAL (KIND=8), INTENT(IN)  :: x_k(:)
       REAL (KIND=8), INTENT(OUT) :: pi_k
     END SUBROUTINE period_profit
     
     SUBROUTINE cum_profit(x_k, pi_k)
       IMPLICIT NONE
       REAL (KIND=8), INTENT(IN)  :: x_k(:)
       REAL (KIND=8), INTENT(OUT) :: pi_k
     END SUBROUTINE cum_profit
  END INTERFACE

  step = 0.0001


  DO j = N-1, k, -1
  
     !backward induction
     !N-1 is de facto the last period, with a termination value V_N=0
     k_dup = j
     IF (j == N-1) THEN
        ! Solve the problem for the grid points of p_{N-1}
        DO i = 1, Q
        i_dup = i
           IF (p_grid(i_dup) > p_bar) THEN
              i_dup = i
              maxf = 500
              x_k(1)= 20.
              first=.true.
              DO
                 CALL minim(x_k, step, nop, object, maxf, iprint, stopcr, nloop,   &
                      iquad, simp, var, period_profit, ier)
                 ! If ier > 0, try a few more function evaluations.
                 IF (ier == 0) EXIT
                 IF (.NOT. first) STOP
                 first = .false.
                 maxf = 100
              END DO
              VF(i,j) = object
              TS(i,j) = x_k(1)
           ELSE
              VF(i,j) = 0.
              TS(i,j) = 0.
           END IF
        END DO
     ELSE
        DO i = 1, Q
        i_dup = i
           IF (p_grid(i_dup) > p_bar) THEN
           i_dup = i
              maxf = 1000
              x_k(1)= 20.
              first=.true.
              DO
                 CALL minim(x_k, step, nop, object, maxf, iprint, stopcr, nloop,   &
                      iquad, simp, var, cum_profit, ier)
                 ! If ier > 0, try a few more function evaluations.
                 IF (ier == 0) EXIT
                 IF (.NOT. first) THEN
                 WRITE(*,*) "Period:",k,"grid:",i
                 STOP
                 END IF
                 
                 first = .false.
                 maxf = 500
              END DO
              VF(i,j) = object
              TS(i,j) = x_k(1)
           ELSE
              VF(i,j) = 0.
              TS(i,j) = 0.
           END IF
        END DO
        
     END IF
     
  END DO

END SUBROUTINE informed
