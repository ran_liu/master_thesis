SUBROUTINE market_maker(v_grid, k)
  USE variables
  IMPLICIT NONE

  INTEGER, INTENT (IN) :: k
  INTEGER :: i, j, m
  REAL (KIND=8), DIMENSION (1:Q), INTENT (IN) :: v_grid
  REAL (KIND=8), DIMENSION (1:Q) :: prob, x
  REAL (KIND=8), DIMENSION (1:Q, 1:N) :: TS !local copy
  REAL (KIND=8) :: temp
  
  TS =  RESHAPE( (/((/(50-(i-1)*10,i=1,Q)/),j=1,N)/), (/Q,N/) )
  TS(:,N) = 0.
  
  DO m = k, N

     DO i = 1, Q
        !solve for x for each element in the v grid
        CALL informed(TS, m)
        WRITE(*,*) "MM i=",i
        CALL get_x(temp, p_h(k-1), TS(:,k))
        x(i) = temp
     END DO
     


     DO i = 1, Q
        ! sweep every point in the y grid
        DO j = 1,Q
        
           prob(j) = EXP( -(y_grid(i)-x(j))**2 / (2*sigma_u**2*deltaT) - &
                (v_grid(j)-v_bar)**2 / (2*sigma_0**2) ) / &
                (2*PI*sigma_u*SQRT(deltaT)*sigma_0)
        END DO
        PR(i, m) = SUM(v_grid * prob)
        
     END DO
     
  END DO
END SUBROUTINE market_maker
