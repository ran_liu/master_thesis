PROGRAM main
  USE variables
  USE arguments
  IMPLICIT NONE

  INTEGER :: status1, i, j, k ! k:index for period
  REAL (KIND=8), PARAMETER :: tol = 10.D-4
  REAL (KIND=8), DIMENSION (:,:), ALLOCATABLE :: TS_pre, PR_pre
  ! Trading strategy (TS), initialised to 20
  REAL (KIND=8), DIMENSION (1:Q, 1:N) :: TS = RESHAPE( (/((/(50-(i-1)*10,i=1,Q)/),j=1,N)/), (/Q,N/) )
  ! PR is put into the variables MODULE as static to save arguments
  REAL (KIND=8), DIMENSION (1:Q) :: v_grid !for the market maker problem
  REAL (KIND=8) :: x_real, u_real

  call timestamp ( )
  v_grid = (/ (-3*sigma_0+v_bar+(i-1)*6*sigma_0/(Q-1), i=1,Q) /)

  TS(:,N) = 0.
  VF(:,N) = 0.


  ! At the beginning of the day, the value of the asset is realised
  CALL rnorm(v, 1, 1)  !simulate standard normal
  v = sigma_0 * v + v_bar 


  DO k = 1, N
     !Outer loop, traversing period (auctions)

     ALLOCATE (TS_pre(Q, N+1-k))
     ALLOCATE (PR_pre(Q, N+1-k))
     TS_pre = TS(:, k:N)
     PR_pre = PR(:, k:N)

     ! draw a sample of u to form total order flow
     ! same u for the iterations
     CALL RANDOM_SEED()
     CALL rnorm(u, R, N)
     u = sigma_u * u * SQRT(deltaT)

     WRITE(*,*) k
     DO
        ! Inner loop, looking for the equilibrium for each auction

        !Start from the informed investor problem
        !Output: Trading strategy (TS)
        CALL informed(TS, k)
        WRITE(*,*) "informed OK"
        !Proceed to the marker maker problem
        CALL market_maker(v_grid, k)
        WRITE(*,*) "market maker OK"

        !Check if we TS and PR converge
        IF ( (MAXVAL(ABS(TS(:,k:N)-TS_pre(:,:))) < tol) .AND. (MAXVAL(ABS(PR(:,k:N)-PR_pre(:,:))) < tol) ) EXIT
        !Record TS and PR for comparison in the next iteration
        TS_pre(:,:) = TS(:,k:N)
        PR_pre(:,:) = PR(:,k:N)
     END DO

     ! record historical price
     CALL get_x(x_real, p_h(k-1), TS(:,k))
     CALL rnorm(u_real, 1, 1)
     u_real = sigma_u * u_real * SQRT(deltaT)
     CALL get_price(p_h(k), x_real+u_real, k)

     DEALLOCATE (TS_pre)
     DEALLOCATE (PR_pre)
  END DO

  OPEN(UNIT=9, FILE='output.csv', STATUS='REPLACE', ACTION='WRITE', IOSTAT=status1)
  IF (status1 > 0) THEN
     WRITE(*,*) "ERROR OPENNING FILE"
  ELSE
     DO i = 1, Q
     DO j = 1, N
        WRITE(9,*) TS(i,j), ",", PR(i,j)
        END DO
     END DO
  END IF
  CLOSE(UNIT=9)

  call timestamp ( )

END PROGRAM main
